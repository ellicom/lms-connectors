lms connector
===============

Package that connects to LMS WebServices that get / add / update data.
For now, there is only one connector for ELMG e-doceo.

INSTALLATION

In order to install this LMS Connectors, just add the following to your composer.json:
    "ellicom/lmsconnectors": "1.*"
    "repositories": [
        {
          "type": "vcs",
          "url": "https://***user***@bitbucket.org/ellicom/lms-connectors.git"
        }
      ]
Then run composer update.


FOR LARAVEL USER:

Open your config/app.php and add the following to the providers array:
    Ellicom\LmsConnectors\LmsConnectorsServiceProvider::class
    
Run the command below to publish the package config file config/lms-connector.php:
    php artisan vendor:publish
    
Open your config/lms-connector.php and add the needed configurations

Then remove from conf/app.php:
    Ellicom\LmsConnectors\LmsConnectorsServiceProvider::class