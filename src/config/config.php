<?php

return [

	'Elmg' => [
		'url' => '',
		'login' => '',
	    'password' => '',

	    /*
		 * The left column must match the key returned by the LMS.
		 * The right column must match the key of your models
		 */
	    'mapping' => [
		    'course' => [
			    'id'          => 'course_id',
			    'name'        => 'title',
			    'title'       => 'title',
			    'date'        => 'date',
			    'valid'       => 'valid',
			    'description' => 'description'
		    ],
		    'email' => [
			    'title'       => 'subject',
			    'body'        => 'body'
		    ],
		    'group' => [
			    'group_id'    => 'group_id',
			    'name'        => 'title'
		    ],
		    'results' => [
			    'id_membre'   => 'user_id',
			    'user_id'     => 'user_id',
			    'id_parcours' => 'course_id',
			    'trainingpath_id' => 'course_id',
			    'sco_id'      => 'learning_unit_id',
			    'id_profil'   => 'session_id',
			    'nb'          => 'attempts',
			    'date'        => 'date',
			    'lesson_status' => 'status',
			    'temps_passe' => 'spent_time',
			    'spent_time'  => 'spent_time',
			    'score'       => 'score'
		    ],
		    'session' => [
			    'id'          => 'session_id',
			    'name'        => 'title',
			    'text'        => 'description',
			    'start_date'  => 'start_date',
			    'end_date'    => 'end_date',
			    'active'      => 'active'
		    ],
		    'user' => [
			    'user_id'     => 'user_id',
			    'login'       => 'user_name',
			    'password'    => 'password',
			    'firstname'   => 'first_name',
			    'lastname'    => 'last_name',
			    'email'       => 'email',
			    'active'      => 'active',
			    'start_date'  => 'start_date',
			    'end_date'    => 'end_date',
			    'level_id'    => 'role_id',
			    'lang'        => 'lang'
		    ]
	    ]
	],

    'models' => [
	    'course'  => App\Models\Course::class,
	    'email'    => App\Models\Email::class,
	    'group'   => App\Models\Group::class,
	    'results' => App\Models\Results::class,
	    'session' => App\Models\Session::class,
	    'user'    => App\Models\User::class
    ]

];