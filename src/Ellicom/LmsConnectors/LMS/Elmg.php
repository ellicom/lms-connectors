<?php

namespace Ellicom\LmsConnectors\LMS;

use Ellicom\LmsConnectors\LmsInterface;
use Ellicom\LmsConnectors\SoapApi;

/**
 * Elmg connector
 *
 */
class Elmg extends LmsInterface { //TODO would be great to add caching capability

	/* @var SoapApi */
	private $api;

	private $baseURL;

    public function __construct() {
	    $url = $this->getConfig('Elmg.url');
	    $this->api = new SoapApi($url, [
		    'login' => $this->getConfig('Elmg.login'),
		    'password' => $this->getConfig('Elmg.password')
	    ]);

	    $scheme = parse_url($url, PHP_URL_SCHEME);
	    $this->baseURL = (empty($scheme) ? 'http://' : $scheme . '://') . parse_url($url, PHP_URL_HOST);

	    return $this;
    }

	/**
	 * Function that add a user to a group
	 *
	 * @param int $userId
	 * @param int $groupId
	 *
	 * @return mixed|null
	 */
	public function addUserToGroup($userId, $groupId) {
		$success = $this->api->call('assocUserIdToGroupId', ['user_id' => $userId, 'group_id' => $groupId]);
		if (empty($success)) {
			return false;
		}

		return $success;
	}

	/**
	 * Function that return all courses
	 *
	 * @return mixed|null
	 */
	public function getCourses() {
		$courses = $this->api->call('getTrainingPathList');
		if (empty($courses)) {
			return null;
		}
		$courses = $this->addImage($courses, $this->baseURL.'/data/{id}/{id}-thumbnail', ['id']);

		return $this->mapToModels($courses, $this->getConfig('models.course'), $this->getConfig('Elmg.mapping.course'));
	}

	/**
	 * Function that return all users for a specified course
	 *
	 * @param int $courseId
	 *
	 * @return mixed[]|null
	 */
	public function getCourseUsers($courseId) {
		$users = $this->api->call('getCourseUsers', ['course_id' => $courseId]);
		if (empty($users)) {
			return null;
		}

		return $this->mapToModels($users, $this->getConfig('models.user'), $this->getConfig('Elmg.mapping.user'));
	}

	/**
	 * Function that return an email from an identifier
	 *
	 * @param mixed  $identifier
	 * @param string $lang
	 *
	 * @return mixed|null
	 */
	public function getEmailTemplate($identifier, $lang = 'fr') {
		if($lang == 'en') {
			$lang = 'uk';
		}

		$email = $this->api->call('getEmailTemplateByReference', ['reference' => $identifier, 'lang' => $lang]);
		if (empty($email)) {
			return null;
		}

		return $this->mapToModel($email, $this->getConfig('models.email'), $this->getConfig('Elmg.mapping.email'));
	}

	/**
	 * Function that return all users with a specified filter ID
	 *
	 * @param int $filterId
	 *
	 * @return mixed[]|null
	 */
	public function getFilteredUsers($filterId) {
		$usersId = $this->api->call('getFiltredUserList', ['filter_id' => $filterId]);
		if (empty($usersId)) {
			return null;
		}
		$users = $this->api->call('getUsersListByIds', ['user_id' => $usersId]);
		if (empty($users)) {
			return null;
		}

		return $this->mapToModels($users, $this->getConfig('models.user'), $this->getConfig('Elmg.mapping.user'));
	}

	/**
	 * Function that return a group from an identifier
	 *
	 * @param int $groupId
	 *
	 * @return mixed|null
	 */
	public function getGroupById($groupId) {
		$group = $this->api->call('getGroupInfosById', ['group_id' => $groupId]);
		if (empty($group)) {
			return null;
		}

		return $this->mapToModel($group, $this->getConfig('models.group'), $this->getConfig('Elmg.mapping.group'));
	}

	/**
	 * Function that return a group from a title
	 *
	 * @param string $title
	 *
	 * @return mixed|null
	 */
	public function getGroupByTitle($title) {
		$group = $this->api->call('getGroupInfosByName', ['group_name' => $title]);
		if (empty($group)) {
			return null;
		}

		return $this->mapToModel($group, $this->getConfig('models.group'), $this->getConfig('Elmg.mapping.group'));
	}

	/**
	 * Function that return all courses for a specified group
	 *
	 * @param int $groupId
	 *
	 * @return mixed[]|null
	 */
	public function getGroupCourses($groupId) {
		$courses = $this->api->call('getGroupCourses', ['group_id' => $groupId]);
		if (empty($courses)) {
			return null;
		}
		$courses = $this->addImage($courses, $this->baseURL.'/data/{id}/{id}-thumbnail', ['id']);

		return $this->mapToModels($courses, $this->getConfig('models.course'), $this->getConfig('Elmg.mapping.course'));
	}

	/**
	 * Function that return all groups
	 *
	 * @return mixed[]|null
	 */
	public function getGroups() {
		$groups = $this->api->call('getGroupsList5_2_7');
		if (empty($groups)) {
			return null;
		}

		return $this->mapToModels($groups, $this->getConfig('models.group'), $this->getConfig('Elmg.mapping.group'));
	}

	/**
	 * Function that return a URL needed to launch a user learning unit
	 *
	 * @param int $userId
	 * @param int $courseId
	 * @param int $scoId
	 * @param string[] $urlParams Associative array of parameters that will be sent back on course closing
	 *
	 * @return string
	 */
	public function getLearningUnitLaunchingURL($userId, $courseId, $scoId, $urlParams = []) {
		$sso_key = $this->getUserSSOKey($userId);

		$http_params = [
			"skey" => $sso_key, // SSO authentication key
			"next_action" => "launchSCO", // Action to perform right after authentication
			"params" => $courseId.';'.$scoId, // Action parameters
			"url_callback" => $this->baseURL.'/plugins/callback.php'.http_build_query($urlParams) // URL to navigate to after the SCO completion
		];

		return $this->baseURL.'/sso.php?' . http_build_query($http_params);
	}

	/**
	 * Function that return all courses for a specified session
	 *
	 * @param int $sessionId
	 *
	 * @return mixed|null
	 */
	public function getSessionCourses($sessionId) {
		$courses = $this->api->call('getCoursesBySessionId', ['session_id' => $sessionId]);
		if (empty($courses)) {
			return null;
		}
		$courses = $this->addImage($courses, $this->baseURL.'/data/{id}/{id}-thumbnail', ['id']);

		return $this->mapToModels($courses, $this->getConfig('models.course'), $this->getConfig('Elmg.mapping.course'));
	}

	/**
	 * Function that return all sessions
	 *
	 * @return mixed[]|null
	 */
	public function getSessions() {
		$sessions = $this->api->call('getSessionsList');
		if (empty($sessions)) {
			return null;
		}
		$sessions = $this->addImage($sessions, $this->baseURL.'/data/images/sessions_thumbs/{id}', ['id']);

		return $this->mapToModels($sessions, $this->getConfig('models.session'), $this->getConfig('Elmg.mapping.session'));
	}

	/**
	 * Function that return a user from an identifier
	 *
	 * @param int $userId
	 *
	 * @return mixed|null
	 */
	public function getUserById( $userId ) {
		$user = $this->api->call('getUserInfosByUserId5_2_1', ['user_id' => $userId]);
		if (empty($user)) {
			return null;
		}

		return $this->mapToModel($user, $this->getConfig('models.user'), $this->getConfig('Elmg.mapping.user'));
	}

	/**
	 * Function that return a user by it's username
	 *
	 * @param string $username
	 *
	 * @return mixed|null
	 */
	public function getUserByUserName( $username ) {
		$user = $this->api->call('getUserInfosByLogin', ['login' => $username]);
		if (empty($user)) {
			return null;
		}

		return $this->mapToModel($user, $this->getConfig('models.user'), $this->getConfig('Elmg.mapping.user'));
	}

	/**
	 * Function that return user results for a specified course
	 *
	 * @param int $userId
	 * @param int $courseId
	 *
	 * @return mixed[]|null
	 */
	public function getUserCourseResults($userId, $courseId) {
		$results = $this->api->call('getCourseResultByUserId', ['user_id' => $userId, 'course_id' => $courseId]);
		if (empty($results)) {
			return null;
		}

		return $this->mapToModels($results, $this->getConfig('models.results'), $this->getConfig('Elmg.mapping.results'));
	}

	/**
	 * Function that return user results
	 *
	 * @param int $userId
	 *
	 * @return mixed[]|null
	 */
	public function getUserResults($userId) {
		$results = $this->api->call('getResultsByUserId', ['user_id' => $userId]);
		if (empty($results)) {
			return null;
		}

		return $this->mapToModels($results, $this->getConfig('models.results'), $this->getConfig('Elmg.mapping.results'));
	}

	/**
	 * Function that return user results for a specified session
	 *
	 * @param int $userId
	 * @param int $sessionId
	 *
	 * @return mixed[]|null
	 */
	public function getUserSessionResults($userId, $sessionId) {
		$results = $this->api->call('getSessionResultByUserId', ['user_id' => $userId, 'session_id' => $sessionId]);
		if (empty($results)) {
			return null;
		}

		return $this->mapToModels($results, $this->getConfig('models.results'), $this->getConfig('Elmg.mapping.results'));
	}

	/**
	 * Function that return all courses for a specified user
	 *
	 * @param int $userId
	 *
	 * @return mixed[]|null
	 */
	public function getUserCourses($userId) {
		$courses = $this->api->call('getUserCourses', ['user_id' => $userId]);
		if (empty($courses)) {
			return null;
		}
		$courses = $this->addImage($courses, $this->baseURL.'/data/{id}/{id}-thumbnail', ['id']);

		return $this->mapToModels($courses, $this->getConfig('models.course'), $this->getConfig('Elmg.mapping.course'));
	}

	/**
	 * Function that return all groups for a specified user
	 *
	 * @param int $userId
	 *
	 * @return mixed[]|null
	 */
	public function getUserGroups($userId) {
		$groups = $this->api->call('getUsersGroupsList', ['user_id' => $userId]);
		if (empty($groups)) {
			return null;
		}

		return $this->mapToModels($groups, $this->getConfig('models.group'), $this->getConfig('Elmg.mapping.group'));
	}

	/**
	 * Function that return all sessions for a specified user
	 *
	 * @param int $userId
	 *
	 * @return mixed[]|null
	 */
	public function getUserSessions($userId) {
		$sessions = $this->api->call('getSessionIdsByUserId', ['user_id' => $userId]);
		if (empty($sessions)) {
			return null;
		}
		$sessions = $this->addImage($sessions, $this->baseURL.'/data/images/sessions_thumbs/{id}', ['id']);

		return $this->mapToModels($sessions, $this->getConfig('models.session'), $this->getConfig('Elmg.mapping.session'));
	}

	/**
	 * Function that return all users
	 *
	 * @return mixed[]|null
	 */
	public function getUsers() {
		$users = $this->api->call('getUsersList5_2_5');
		if (empty($users)) {
			return null;
		}

		return $this->mapToModels($users, $this->getConfig('models.user'), $this->getConfig('Elmg.mapping.user'));
	}

	/**
	 * Function that return a user SSO key
	 *
	 * @param int $userId
	 *
	 * @return string
	 */
	protected function getUserSSOKey($userId) {
		return $this->api->call('createSSOSecurityKey', ['uid' => $userId]);
	}

	/**
	 * Function that return a user SSO key
	 *
	 * @param $username
	 *
	 * @return string
	 */
	protected function getUserSSOKeyByUsername($username) {
		return $this->api->call('createSSOSecurityKeyByLogin', ['login' => $username]);
	}

	/**
	 * Function that return if a username exists
	 *
	 * @param string $username
	 *
	 * @return boolean
	 */
	public function isUsernameExists($username) {
		return $this->api->call('loginExists', ['login' => $username]);
	}

	/**
	 * Function that remove a user from a group
	 *
	 * @param int $userId
	 * @param int $groupId
	 *
	 * @return mixed|null
	 */
	public function removeUserFromGroup($userId, $groupId) {
		$group = $this->getGroupById($groupId);

		$groupMapping = $this->getConfig('Elmg.mapping.group');
		if(isset($groupMapping['name'])) {
			$groupName = $group->{$groupMapping['name']};
		}
		else {
			$groupName = $group->name;
		}

		$success = $this->api->call('unregisterUserToGroup', ['user_id' => $userId, 'group_name' => $groupName]);
		if (empty($success)) {
			return false;
		}

		return $success;
	}

	/**
	 * Function that update a user
	 *
	 * @param User $user
	 *
	 * @return boolean
	 */
	public function updateUser($user) {
		$lmsUser = $this->mapFromModel($user->getAttributes(), $this->getConfig('Elmg.mapping.user'));

		return !is_null($this->api->call('setUserInfosByUserId', [
			'user_id' => $lmsUser['user_id'],
			'login' => $lmsUser['login'],
			'pass' => isset($lmsUser['password']) ? $lmsUser['password'] : null,
			'firstname' => isset($lmsUser['firstname']) ? $lmsUser['firstname'] : null,
			'lastname' => isset($lmsUser['lastname']) ? $lmsUser['lastname'] : null,
			'email' => isset($lmsUser['email']) ? $lmsUser['email'] : null,
			'start_date' => $this->dateToTimestamp($lmsUser['start_date']),
			'end_date' => $this->dateToTimestamp($lmsUser['end_date']),
			'style' => isset($lmsUser['style']) ? $lmsUser['style'] : null,
			'level_id' => isset($lmsUser['level_id']) ? $lmsUser['level_id'] : null,
			'service' => isset($lmsUser['service']) ? $lmsUser['service'] : null,
			'active' => isset($lmsUser['active']) ? $lmsUser['active'] : null,
			'field1' => isset($lmsUser['field1']) ? $lmsUser['field1'] : null,
			'field2' => isset($lmsUser['field2']) ? $lmsUser['field2'] : null,
			'field3' => isset($lmsUser['field3']) ? $lmsUser['field3'] : null,
			'field4' => isset($lmsUser['field4']) ? $lmsUser['field4'] : null,
			'field5' => isset($lmsUser['field5']) ? $lmsUser['field5'] : null,
			'field6' => isset($lmsUser['field6']) ? $lmsUser['field6'] : null,
			'field7' => isset($lmsUser['field7']) ? $lmsUser['field7'] : null,
			'field8' => isset($lmsUser['field8']) ? $lmsUser['field8'] : null,
			'field9' => isset($lmsUser['field9']) ? $lmsUser['field9'] : null,
			'field10' => isset($lmsUser['field10']) ? $lmsUser['field10'] : null,
			'field11' => isset($lmsUser['field11']) ? $lmsUser['field11'] : null,
			'field12' => isset($lmsUser['field12']) ? $lmsUser['field12'] : null,
			'field13' => isset($lmsUser['field13']) ? $lmsUser['field13'] : null,
			'field14' => isset($lmsUser['field14']) ? $lmsUser['field14'] : null,
			'field15' => isset($lmsUser['field15']) ? $lmsUser['field15'] : null,
			'field16' => isset($lmsUser['field16']) ? $lmsUser['field16'] : null,
			'field17' => isset($lmsUser['field17']) ? $lmsUser['field17'] : null,
			'field18' => isset($lmsUser['field18']) ? $lmsUser['field18'] : null,
			'field19' => isset($lmsUser['field19']) ? $lmsUser['field19'] : null,
			'field20' => isset($lmsUser['field20']) ? $lmsUser['field20'] : null,
			'lang' => isset($lmsUser['lang']) ? $lmsUser['lang'] : null,
			'sendmail' => isset($lmsUser['sendmail']) ? $lmsUser['sendmail'] : false,
			'mailsender' => isset($lmsUser['mailsender']) ? $lmsUser['mailsender'] : '',
			'auth_method' => isset($lmsUser['auth_method']) ? $lmsUser['auth_method'] : ''
		]));
	}


	private function dateToTimestamp($date, $format = "d/m/Y") {
		if(!isset($date) || empty($date)) {
			return null;
		}

		$timeStamp = \DateTime::createFromFormat($format, $date)->getTimestamp();
		return ($timeStamp > 0) ? $timeStamp : null;
	}


	private function addImage($data, $url, $replacement) {
		$array = true;
		if(!is_array($data)) {
			$array = false;
			$data = [$data];
		}

		$newData = [];
		foreach($data as $model) {
			foreach($replacement as $r) {
				$model->image = $this->getFullImageUrl(str_replace('{'.$r.'}', $model->{$r}, $url));
				$newData[] = $model;
			}
		}

		if(!$array) {
			return $newData[0];
		}
		return $newData;
	}

	private function getFullImageUrl($url) {
		$extensions = ['jpg', 'jpeg', 'png', 'gif'];
		foreach($extensions as $ext) {
			if($this->remoteFileExists("$url.$ext")) {
				return "$url.$ext";
			}
		}

		return null;
	}

	private function remoteFileExists($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		// don't download content
		curl_setopt($ch, CURLOPT_NOBODY, 1);
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		return curl_exec( $ch ) !== FALSE;
	}
}