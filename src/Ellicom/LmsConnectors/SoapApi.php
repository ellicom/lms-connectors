<?php

namespace Ellicom\LmsConnectors;

use SoapClient;
use Exception;

class SoapApi {

	/* @var SoapClient */
	private $client;

	private $soapURL;
	private $soapParams;

	/**
	 * SoapApi constructor. Create Soap connection using passed parameters.
	 *
	 * @param string   $soapURL
	 * @param string[] $soapParams
	 */
	public function __construct($soapURL, $soapParams) {
		ini_set('default_socket_timeout', 120);

		$this->soapURL = $soapURL;
		$this->soapParams = $soapParams;
		$this->connect();
	}

	private function connect() {
		try {
			$this->client = new SoapClient($this->soapURL, $this->soapParams);
		} catch ( Exception $e ) {
			throw new Exception("Connection failed: " . $e->getMessage());
		}
	}

	/**
	 * Function that make the final call to the external service using soap
	 * client.
	 *
	 * @param string  $func Function name to be called
	 * @param mixed[] $params Arguments needed for the call
	 * @param bool    $exception If we should throw an exception on error
	 * @param bool    $fromFault
	 *
	 * @return mixed|mixed[] $soapParams*
	 * @throws Exception
	 */
	public function call($func, $params = [], $exception = FALSE, $fromFault = FALSE) {
		try {
			$soapResult = $this->client->__soapCall($func, $params);
		}
		catch(\SoapFault $e) {
			if(!$fromFault) {
				$this->connect();
				return $this->call($func, $params, $exception, true);
			}
			$soapResult = null;
		}
		catch(\Exception $e) {
			$soapResult = null;
		}

		if (!is_null($soapResult)) {
			return $soapResult;
		}
		else {
			if ($exception == TRUE) {
				throw new Exception("Unexpected soapResult for {$func}(" . var_export($params,
						TRUE) . "):\n" . var_export($soapResult, TRUE));
			}
			else {
				return [];
			}
		}
	}

}