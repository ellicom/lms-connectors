<?php

namespace Ellicom\LmsConnectors;

/**
 * Class LmsInterface
 * This class is used as an interface so every LMS implementation will have the
 * same functions definitions. This way, we can use this plugin with any LMS
 * implementation the same way.
 **/
abstract class LmsInterface {

	/**
	 * Function that add a user to a group
	 *
	 * @param int $userId
	 * @param int $groupId
	 *
	 * @return mixed|null
	 */
	abstract public function addUserToGroup($userId, $groupId);

	/**
	 * Function that return all courses
	 *
	 * @return mixed|null
	 */
	abstract public function getCourses();

	/**
	 * Function that return all users for a specified course
	 *
	 * @param int $courseId
	 *
	 * @return mixed[]|null
	 */
	abstract public function getCourseUsers($courseId);

	/**
	 * Function that return an email from an identifier
	 *
	 * @param mixed  $identifier
	 * @param string $lang
	 *
	 * @return mixed|null
	 */
	abstract public function getEmailTemplate($identifier, $lang = 'fr');

	/**
	 * Function that return a group from an identifier
	 *
	 * @param int $groupId
	 *
	 * @return mixed|null
	 */
	abstract public function getGroupById($groupId);

	/**
	 * Function that return a group from a title
	 *
	 * @param string $title
	 *
	 * @return mixed|null
	 */
	abstract public function getGroupByTitle($title);

	/**
	 * Function that return all courses for a specified group
	 *
	 * @param int $groupId
	 *
	 * @return mixed[]|null
	 */
	abstract public function getGroupCourses($groupId);

	/**
	 * Function that return all groups
	 *
	 * @return mixed[]|null
	 */
	abstract public function getGroups();

	/**
	 * Function that return a URL needed to launch a user learning unit
	 *
	 * @param int $userId
	 * @param int $courseId
	 * @param int $scoId
	 * @param string[] $urlParams Associative array of parameters that will be sent back on course closing
	 *
	 * @return string
	 */
	abstract public function getLearningUnitLaunchingURL($userId, $courseId, $scoId, $urlParams = []);

	/**
	 * Function that return all courses for a specified session
	 *
	 * @param int $sessionId
	 *
	 * @return mixed|null
	 */
	abstract public function getSessionCourses($sessionId);

	/**
	 * Function that return all sessions
	 *
	 * @return mixed[]|null
	 */
	abstract public function getSessions();

	/**
	 * Function that return a user from an identifier
	 *
	 * @param int $userId
	 *
	 * @return mixed|null
	 */
	abstract public function getUserById($userId);

    /**
     * Function that return a user by it's username
     *
     * @param string $username
     *
     * @return mixed|null
     */
    abstract public function getUserByUserName($username);

	/**
	 * Function that return user results for a specified course
	 *
	 * @param int $userId
	 * @param int $courseId
	 *
	 * @return mixed[]|null
	 */
	abstract public function getUserCourseResults($userId, $courseId);

	/**
	 * Function that return user results for a specified session
	 *
	 * @param int $userId
	 * @param int $sessionId
	 *
	 * @return mixed[]|null
	 */
	abstract public function getUserSessionResults($userId, $sessionId);

	/**
	 * Function that return user results
	 *
	 * @param int $userId
	 *
	 * @return mixed[]|null
	 */
	abstract public function getUserResults($userId);

	/**
	 * Function that return all courses for a specified user
	 *
	 * @param int $userId
	 *
	 * @return mixed[]|null
	 */
	abstract public function getUserCourses($userId);

	/**
	 * Function that return all groups for a specified user
	 *
	 * @param int $userId
	 *
	 * @return mixed[]|null
	 */
	abstract public function getUserGroups($userId);

	/**
	 * Function that return all sessions for a specified user
	 *
	 * @param int $userId
	 *
	 * @return mixed[]|null
	 */
	abstract public function getUserSessions($userId);

	/**
	 * Function that return all users
	 *
	 * @return mixed[]|null
	 */
	abstract public function getUsers();

	/**
	 * Function that return a user SSO key
	 *
	 * @param int $userId
	 *
	 * @return string
	 */
	abstract protected function getUserSSOKey($userId);

	/**
	 * Function that return a user SSO key
	 *
	 * @param $username
	 *
	 * @return string
	 */
	abstract protected function getUserSSOKeyByUsername($username);

	/**
	 * Function that return if a username exists
	 *
	 * @param string $username
	 *
	 * @return boolean
	 */
	abstract public function isUsernameExists($username);

	/**
	 * Function that remove a user from a group
	 *
	 * @param int $userId
	 * @param int $groupId
	 *
	 * @return mixed|null
	 */
	abstract public function removeUserFromGroup($userId, $groupId);


	/**
	 * Create a LMS model from an application model and a mapping.
	 *
	 * @param mixed    $model
	 * @param string[] $mapping
	 *
	 * @return mixed|null
	 */
	protected function mapFromModel($model, $mapping) {
		$LmsModel = [];

		foreach ($model as $key => $value) {
			$moduleKeys = array_keys($mapping, $key);
			if(count($moduleKeys) > 0) {
				foreach ($moduleKeys as $moduleKey) {
					$LmsModel[$moduleKey] = $value;
				}
			}
			else {
				$LmsModel[$key] = $value;
			}
		}

		return $LmsModel;
	}

	/**
	 * Create a model from lms results and a mapping.
	 *
	 * @param mixed[]  $result
	 * @param mixed    $model
	 * @param string[] $mapping
	 *
	 * @return mixed|null
	 */
	protected function mapToModel($result, $model, $mapping) {
		$model = new $model;

		foreach ($result as $key => $value) {
			if (array_key_exists($key, $mapping)) {
				$keyMap = $mapping[$key];
				if ($keyMap) {
					$model->$keyMap = $value;
				}
			}
			else {
				$model->$key = $value;
			}
		}

		return $model;
	}

	/**
	 * Create models from lms results and a mapping.
	 *
	 * @param mixed[][] $results
	 * @param mixed     $model
	 * @param string[]  $mapping
	 *
	 * @return mixed[]
	 */
	protected function mapToModels($results, $model, $mapping) {
		$models = [];

		foreach ($results as $result) {
			$models[] = $this->mapToModel($result, $model, $mapping);
		}

		return $models;
	}

	/**
	 * Function that return a configuration by its name (using dot syntax)
	 *
	 * @param string $configName
	 * @param mixed $default
	 *
	 * @return mixed
	 */
	protected function getConfig($configName, $default = null) {
		if(function_exists('config')) {
			if (is_null($configName)) {
				return config("lms-connector", $default);
			}
			return config("lms-connector.$configName", $default);
		}
		else {
			$config = include __DIR__.'/../../config/config.php';

			//Get value using dot notation
			if (is_null($configName)) {
				return $config;
			}
			if (isset($config[$configName])) {
				return $config[$configName];
			}

			foreach (explode('.', $configName) as $segment) {
				if ( !is_array($config) || !array_key_exists($segment, $config)) {
					return $default;
				}

				$config = $config[$segment];
			}

			return $config;
		}
	}
}