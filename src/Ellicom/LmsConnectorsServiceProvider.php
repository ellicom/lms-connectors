<?php namespace Ellicom\LmsConnectors;

use Illuminate\Support\ServiceProvider;

class LmsConnectorsServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		// Publish config files
		$this->publishes([
			__DIR__.'/../config/config.php' => app()->basePath() . '/config/lms-connector.php',
		]);
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->mergeConfig();
	}

	/**
	 * Merges user's and entrust's configs.
	 *
	 * @return void
	 */
	private function mergeConfig()
	{
		$this->mergeConfigFrom(
			__DIR__.'/../config/config.php', 'lms-connector'
		);
	}
}